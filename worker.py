import os
import re
import sys
from pkg_resources import load_entry_point


if __name__ == '__main__':
    sys.path += [os.path.dirname(__file__)]
    sys.argv = [
        re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0]),
        '-A',
        'application.celery',
        'worker',
        '-l',
        'INFO'
    ]
    sys.exit(load_entry_point('celery==3.1.18', 'console_scripts', 'celery')())