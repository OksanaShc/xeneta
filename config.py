import os

basedir = os.path.abspath(os.path.dirname(__file__))

# forms
WTF_CSRF_ENABLED = True
SECRET_KEY = 'hjkhr847545hi'

# database
MONGO_DBNAME = 'terminal'
MONGO_URI = 'mongodb://localhost:27017/terminal'

#  celery
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'


