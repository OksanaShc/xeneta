import os
import unittest
import time
import json
from datetime import datetime
from pymongo import MongoClient
from application import app
from application.helpers.upload_charges import UploadCharges

filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'test_data.zip')


class TestCaseUploadByScript(unittest.TestCase):
    test_data = [
        {'port': 'QWERT', 'currency': 'QW', 'value': 12, 'supplier_id': 123},
        {'port': 'QWERT', 'currency': 'QWE', 'value': 12, 'supplier_id': 127},
        {'port': 'QWERT', 'currency': 'QWE', 'value': 12, 'supplier_id': 11},
    ]

    def setUp(self):
        mongo = MongoClient()
        self.db = mongo.terminal.charges

    def test_upload_from_file(self):
        before = list(self.db.find({'supplier_id': {'$in': [11, 123, 127]}}))
        assert len(before) == 0, 'Test data is already in db'
        try:
            UploadCharges().save_data_from_file(filename)
            result = list(self.db.find({'supplier_id': {'$in': [11, 123, 127]}}))
            assert len(result) == 3, 'Incorrect insert data to db, expected : 3 rows inserted, %s actual' % len(result)
        finally:
            self.db.delete_many({'supplier_id': {'$in': [11, 123, 127]}})

    def test_upload_data(self):
        before = list(self.db.find({'supplier_id': {'$in': [11, 123, 127]}}))
        assert len(before) == 0, 'Test data is already in db'
        try:
            UploadCharges().save_data(self.test_data)
            result = list(self.db.find({'supplier_id': {'$in': [11, 123, 127]}}))
            assert len(result) == 3, 'Incorrect insert data to db, expected : 3 rows inserted, %s actual' % len(result)
        finally:
            self.db.delete_many({'supplier_id': {'$in': [11, 123, 127]}})

    def test_normalize_data(self):
        row = {'port': 'QWERT', 'currency': 'USD', 'value': 12, 'supplier_id': 123}
        result = UploadCharges().normalize_row(row)
        assert result.get('country') == 'QW', 'Incorrect country, Expected: QW, actual: %s' % result.get('country')
        assert result.get('city') == 'ERT', 'Incorrect city, Expected: ERT, actual: %s' % result['city']
        assert result['Date'] == datetime.now().strftime('%Y-%m-%d'),\
            'Incorrect date, Expected: %s, actual: %s' % (datetime.now().strftime('%Y-%m-%d'), result[''])

    def test_validate_correct_data(self):
        row = {'port': 'QWERT', 'currency': 'USD', 'value': 12, 'supplier_id': 123}
        result = UploadCharges().validate_data([row])
        assert result[0]['outlier'] is False

    def test_validate_incorrect_data(self):
        row = {'port': 'QWERTY', 'currency': 'USDL', 'value': 12, 'supplier_id': 123}
        result = UploadCharges().validate_data([row])
        assert result[0]['outlier'] is True


class TestCaseUploadWithApp(unittest.TestCase):
    test_data = [
        {'port': 'QWERT', 'currency': 'QW', 'value': 12, 'supplier_id': 123},
        {'port': 'QWERT', 'currency': 'QWE', 'value': 12, 'supplier_id': 127},
        {'port': 'QWERT', 'currency': 'QWE', 'value': 12, 'supplier_id': 11},
    ]

    def setUp(self):
        mongo = MongoClient()
        self.db = mongo.terminal.charges
        self.app = app.test_client()
        self.db.drop()

    def tearDown(self):
        self.db.drop()

    def test_upload_data(self):

        r = self.app.post('/charges', data=json.dumps({'items': self.test_data}), content_type='application/json')
        assert r.status_code == 202
        task_id = json.loads(r.data.decode('utf-8')).get('taskid')
        assert task_id
        i = 0
        while True:
            r = self.app.get('/status/%s' % task_id)
            r_data = json.loads(r.data.decode('utf-8'))
            if r_data.get('status') == 'SUCCESS':
                break
            assert i < 5, 'Task was not succeed after 5 sec of waiting'
            i += 1
            time.sleep(1)
        result = list(self.db.find({'supplier_id': {'$in': [11, 123, 127]}}))
        assert len(result) == 3, 'Incorrect insert data to db, expected : 3 rows inserted, %s actual' % len(result)

if __name__ == '__main__':
    unittest.main()
