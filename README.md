System required: MongoDB, Redis, Python 3.5
------------------------
Install required packages 
------------------------
```sh
$ sudo pip3.5 install -r requirements.txt

```
Run Worker:
-------------------------
Worker uses Rabbit as broker and backend

```sh
$ python3.5 worker.py &
```

Run Tests:
-------------------------
```sh
$ python3.5 run_tests.py
```

Upload initial data to database:
-------------------------
```sh
$ python3.5 init_app.py
```

Run Server:
-------------------------
Server depends on worker, run worker before running server

```sh
$ python3.5 run.py
```
New entries will be added by POST-request with data in json format
{'items' : [list of entries]}) to endpoint '/charges'.

 