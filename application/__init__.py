from flask import Flask
from flask_login import LoginManager
from flask_pymongo import PyMongo
from celery import Celery

app = Flask(__name__)
app.config.from_object('config')

mongo = PyMongo(app)
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

from . import controllers
