from flask import render_template, url_for, redirect, request
from flask_login import login_required
from flask_login import current_user
from application import app, mongo
from application.forms import SearchForm


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    info = list(mongo.db.charges.aggregate(
        [{'$group': {'_id': {'country': "$country", 'outlier': '$outlier'}, 'count': {'$sum': 1}}}]))
    data = {}
    for row in info:
        country = row['_id'].get('country', None)
        data.setdefault(country, {'country': country, 'outlier': 0, 'normal': 0}).update(
            {'outlier' if row['_id']['outlier'] else 'normal': row['count']})

    return render_template("index.html",
                           title='Home',
                           user=current_user,
                           data=list(data.values()), form=SearchForm())


@app.route('/search', methods=['POST'])
@login_required
def search():
    form = SearchForm()
    if form.validate_on_submit():
        data = list(mongo.db.charges.find({'country': form.search.data}, projection={'_id': 0}))
        return render_template("country.html",
                               title='Home',
                               user=current_user,
                               data=data)
    return redirect(request.args.get('next') or url_for('index'))


@app.route('/index/<string:country>', methods=['GET'])
@login_required
def get_country_values(country):
    data = list(mongo.db.charges.find({'country': country},fields={'_id': 0} ))
    return render_template("country.html",
                           title='Home',
                           user=current_user,
                           data=data)
