from flask import render_template, redirect, url_for, request, flash
from flask_login import login_user, logout_user
from application import app, mongo, lm
from application.forms import LoginForm
from application.models import User


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = mongo.db.users.find_one({'login': form.login.data})

        if user is None:
            flash('No such user! Please register.')
            return redirect(url_for('register'))

        if user['password'] == form.password.data:
            flash('Logged in!')
            user_obj = User(user['login'])
            login_user(user_obj, remember=form.remember_me.data)
            return redirect(request.args.get('next') or url_for('index'))
        else:
            flash('Invalid password!')
            return redirect(url_for('login'))

    return render_template('login.html',
                           title='Sign In',
                           form=form)


@lm.user_loader
def load_user(login):
    user = mongo.db.users.find_one({'login': login})
    if user:
        return User(login)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))
