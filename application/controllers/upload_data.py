from flask import request, jsonify
from application import app, celery
from ..helpers.upload_charges import UploadCharges


@celery.task
def save_data(data):
    UploadCharges().save_data(data)


@app.route('/charges', methods=['POST'])
def upload_charges():
    try:
        r = request.get_json()
        data = r.get('items', [])
        task = save_data.delay(data)
        return jsonify(name=app.name, status='Task is running', taskid=task.id), 202
    except Exception as e:
        print(e)
        return jsonify(name=app.name, status='Exception, \n %s' % e), 400


@app.route('/status/<task_id>')
def get_task_status(task_id):
    try:
        task = save_data.AsyncResult(task_id)
        return jsonify(status=task.status, task=task.task_name)
    except Exception as e:
        return jsonify(status='Exception, \n %s' % e), 400