from flask import render_template, redirect, url_for, flash

from application import app, mongo
from application.forms import RegisterForm


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user = mongo.db.users.find_one({'login': form.login.data})
        if user is None:
            mongo.db.users.insert({'login': form.login.data, 'name': form.name.data,
                                   'email': form.email.data,
                                   'password': form.password.data})
        else:
            flash('User {} already exists!'.format(form.login.data))
            return redirect(url_for('login'))

        flash('Thank you for registration! You can log in now.')
        return redirect(url_for('login'))

    return render_template('register.html',
                           title='Registration',
                           form=form)
