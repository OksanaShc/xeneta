import zipfile
import json
import requests
from jsonschema import validate
from datetime import datetime
from pymongo import MongoClient

mongo = MongoClient()
db = mongo.terminal
api_key = '2aaef437e773467e87e4b33fe76bc428'
url = 'https://openexchangerates.org/api/latest.json?app_id=%s' % api_key

schema = {
    'type': 'object',
    "properties": {
        'port': {"type": "string", "pattern": "^[a-zA-Z]{5}$"},
        'supplier_id': {"type": "number"},
        'currency': {"type": "string", "pattern": "^[a-zA-Z]{3}$"},
        'value': {"type": "number"}
    },
    "required": ["port", "supplier_id", "currency", "value"]
}


class UploadCharges(object):
    def __init__(self):
        self.currency_hash = self.get_currency_rate()

    @staticmethod
    def get_currency_rate():
        now = datetime.now().strftime('%Y-%m-%d')
        db_data = db.currency.find_one({'Date': now})
        if not db_data:
            r = requests.get(url)
            if r.status_code == '400':
                db_data = {}
            else:
                db_data = r.json().get('rates', {})
                db.currency.insert({'Date': now, **db_data})
        return db_data

    def normalize_row(self, row):
        currency = row.get('currency', '')
        if currency:
            row['currency'] = currency.upper()
        if currency != 'USD' and self.currency_hash.get(currency):
            if row.get('value'):
                row['value'] = round(row['value'] / self.currency_hash.get(currency), 2)
                row['currency'] = 'USD'
        port = row.get('port')
        if port and len(port) == 5:
            row['country'] = port[:2]
            row['city'] = port[2:]
        row['Date'] = datetime.now().strftime('%Y-%m-%d')
        return row

    @staticmethod
    def validate_data(data):
        for row in data:
            try:
                validate(row, schema)
                row['outlier'] = False
            except Exception as e:
                row['outlier'] = True
        return data

    def save_data(self, data):

        self.validate_data(data)
        for row in data:
            self.normalize_row(row)

        for i, row in enumerate(data):
            self.normalize_row(row)
            if i % 100 == 0:
                if i > 0:
                    bulk.execute()
                bulk = db.charges.initialize_unordered_bulk_op()
            bulk.insert(row)
        bulk.execute()

    def save_data_from_file(self, file_path):
        with zipfile.ZipFile(file_path, "r") as z:
            for filename in z.namelist():
                print(filename)
                with z.open(filename) as f:
                    data = f.read()
                    data = json.loads(data.decode("utf-8"))
                    self.save_data(data)
