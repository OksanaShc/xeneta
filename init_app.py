import os
import pymongo
from application.helpers.upload_charges import UploadCharges

filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tests', 'data', 'sample_data__1___1_.zip')

if __name__ == '__main__':
    mongo = pymongo.MongoClient()
    mongo.terminal.users.insert({'login': 'admin', 'password': 'admin1'})
    UploadCharges().save_data_from_file(filename)

